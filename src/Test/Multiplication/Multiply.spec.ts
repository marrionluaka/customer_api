import { expect } from 'chai';
import { multiply } from './Multiply';

describe('Multiply specs', function () {
    let multiplyBy321: (val: number) => number;
    
    beforeEach(() => {
        multiplyBy321 = multiply(321);
    });
    
    it('returns zero when b equals zero.', function () {
      const expected = 0;
      
      const actual = multiplyBy321(0);
      
      expect(actual).to.equal(expected);
    });
    
    context("returns product by decrementing the multiplier by one.", () => {
        it('multiply by one', function () {
          const actual = multiplyBy321(1);
      
          expect(actual).to.equal(321);
        });
      
      it('multiply by five', function () {
          const actual = multiplyBy321(5);
      
          expect(actual).to.equal(1605);
        });
    });
    
    it('returns negative one when invalid inputs are passed in.', function () {
        expect(multiplyBy321(null)).to.equal(-1);
    });
  });