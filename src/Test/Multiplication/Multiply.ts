export function multiply(multiplier: number): (val: number) => number { 
	return function by(value: number): number{
    return (function m(a: number, b: number): any{
    	if(typeof a !== 'number') return -1;
    	
      if (b === 0) return 0; 
  		
      return b > 0 ? (a + m(a, b - 1)) : -1; 
    }(value, multiplier));
  };
}