import { expect } from 'chai';
import * as sinon from 'sinon';
import { ICustomerService } from '../../Services/Interfaces/ICustomerService';
import { CustomerService } from '../../Services/Customer.service';
import Customer from '../../Models/Customer';
import CustomerAddress from '../../Models/CustomerAddress';
import { IDbContext } from '../../Services/Interfaces/IDbContext';
import { customersList, customersWithAddresses } from '../TestHelpers';

describe('UserRepository specs', () => {
    let sut: ICustomerService;
    let customers: Array<Customer>;

    beforeEach(() => {
        const dbContextMock: IDbContext = {
            getCustomers: sinon.stub().returns(customersList),

            getCustomerById: sinon.stub().returns(customersList),

            getCustomersWithAddress: sinon.stub().returns(customersWithAddresses)
        };

        sut = new CustomerService(dbContextMock);
        customers = [ new Customer("Ronaldo", null) ];
    });

    it("getAllCustomers(): returns an array of customers.", async() => {
        const actual = await sut.getAllCustomers();

        expect(actual).to.eql(customers);
    });

    it("getCustomerById(): retrieves a customer by its id.", async () => {
        const customer = await sut.getCustomerById("1");

        expect(customer).to.eql(customers[0]);
    });

    it("getCustomersWithAddresses(): returns an array of customers that have an address", async () => {
        const customers = await sut.getCustomersWithAddresses();
        const c = customersWithAddresses[0];

        expect(customers).to.eql([
            new Customer(
                c.name,
                [
                    new CustomerAddress(
                        c.country, 
                        c.postal_code, 
                        c.street_address
                    )
                ]
            )
        ]);
    });
});