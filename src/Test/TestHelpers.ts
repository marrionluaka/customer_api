export const customersList = [
    {
        id: 1,
        name: 'Ronaldo'
    }
];

export const customersWithAddresses = [
    {
        id: 2,
        name: 'Messi',
        country: 'AR', 
        postal_code: 12345, 
        street_address: '187 barcelona sucks'
    }
];