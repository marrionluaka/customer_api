import Customer from "../../Models/Customer";

export interface ICustomerService{
    getAllCustomers(): Promise<Array<Customer>>;
    getCustomerById(id: string): Promise<Customer>;
    getCustomersWithAddresses(): Promise<Array<Customer>>;
}