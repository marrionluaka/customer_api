export interface IDbContext{
    getCustomers(): any;
    getCustomerById(id: number): any;
    getCustomersWithAddress(): any;
}