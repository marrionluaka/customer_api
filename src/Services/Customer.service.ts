import "reflect-metadata";
import { injectable, inject } from 'inversify';
import { ICustomerService } from './Interfaces/ICustomerService';
import Customer from "../Models/Customer";
import { TYPES } from "../types";
import { IDbContext } from "./Interfaces/IDbContext";
import CustomerAddress from "../Models/CustomerAddress";

@injectable()
export class CustomerService implements ICustomerService{
    private readonly _context: IDbContext;

    constructor(@inject(TYPES.IDbContext) context: IDbContext){
        this._context = context;
    }

    public async getAllCustomers(): Promise<Array<Customer>> {
        const customers: Array<any> = await this._context.getCustomers();

        return customers.map((customer: any) => {
            return new Customer(customer.name, this.getCustomerAddress(customer));
        });
    }

    public async getCustomerById(id: string): Promise<Customer | any> {
        const customer: Array<any> = await this._context.getCustomerById(~~id);

        return new Customer(customer[0].name, this.getCustomerAddress(customer[0]));   
    }

    public async getCustomersWithAddresses(): Promise<Array<Customer>> {
        const customersWithAddress: Array<any> = await this._context.getCustomersWithAddress();
        
        return customersWithAddress.map((c: any) => {
            return new Customer(
                c.name,
                [
                    new CustomerAddress(
                        c.country, 
                        c.postal_code, 
                        c.street_address
                    )
                ]
            );
        });
    }

    private getCustomerAddress(c: any): Array<CustomerAddress> | null {
        return !c.street_address ? null : [
            new CustomerAddress(
                c.country, 
                c.postal_code, 
                c.street_address
            )
        ];
    }
}