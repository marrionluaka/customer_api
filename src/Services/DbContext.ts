import { injectable } from "inversify";
import * as pgp from 'pg-promise';
import * as promise from 'bluebird';

import { IDbContext } from "./Interfaces/IDbContext";

@injectable()
export default class DbContext implements IDbContext {
    private readonly _db: any;
    private readonly _limit: number = 10;

    constructor(){
        var connectionString = 'postgres://postgres:root@localhost/customerdb';
        this._db = pgp({ promiseLib: promise, noWarnings: true })(connectionString);
    }

    public async getCustomers(): Promise<Array<any>> {
        return await this._db.any(`
            SELECT * 
            FROM customers AS c
            FULL JOIN customer_addresses AS ca
            ON c.Id = ca.Customer_Id
            LIMIT ${this._limit};
        `);
    }

    public async getCustomerById(id: number): Promise<Array<any>> {
        return await this._db.any(`
            SELECT * 
            FROM customers AS c
            FULL OUTER JOIN customer_addresses AS ca
            ON c.Id = ca.Customer_Id 
            WHERE c.Id = $1 
            LIMIT ${this._limit};
        `, id);
    }

    public async getCustomersWithAddress(): Promise<Array<any>> {
        return await this._db.any(`
            SELECT C.NAME, CA.Street_Address, CA.Postal_Code, CA.Country
            FROM customer_addresses AS CA
            LEFT JOIN customers AS C ON CA.Customer_Id = C.ID
            LIMIT ${this._limit};
        `);
    }
}