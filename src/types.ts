const TYPES = { 
    ICustomerService: Symbol.for("ICustomerService"),
    IDbContext: Symbol.for("IDbContext"),
}

export { TYPES };