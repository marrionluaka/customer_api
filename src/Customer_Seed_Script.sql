DROP DATABASE IF EXISTS customerdb;
CREATE DATABASE customerdb;

\c customerdb

DROP TABLE IF EXISTS Customers CASCADE;
CREATE TABLE Customers 
(
    ID      SERIAL NOT NULL PRIMARY KEY,
    NAME    VARCHAR NOT NULL
);

INSERT INTO Customers (NAME) VALUES ('Ryan');
INSERT INTO Customers (NAME) VALUES ('Jonathan');
INSERT INTO Customers (NAME) VALUES ('Colin');
INSERT INTO Customers (NAME) VALUES ('Syed');

SELECT * FROM Customers;

DROP TABLE IF EXISTS Customer_Addresses;
CREATE TABLE Customer_Addresses
(
	ID              SERIAL NOT NULL PRIMARY KEY,
    Customer_Id     INTEGER REFERENCES Customers(ID),
	Street_Address  VARCHAR(255),
	Postal_Code     NUMERIC (5, 0),
	Country         CHAR(2)
);

INSERT INTO Customer_Addresses (Customer_Id, Street_Address, Postal_Code, Country) 
VALUES (2, '123 Big Walk Way', 75023, 'US');

INSERT INTO Customer_Addresses (Customer_Id, Street_Address, Postal_Code, Country) 
VALUES (3, '509 Charter Road', 90021, 'US');

INSERT INTO Customer_Addresses (Customer_Id, Street_Address, Postal_Code, Country) 
VALUES (1, '999 Night Stalker Road', 12345, 'US');

SELECT * FROM Customer_Addresses;

SELECT C.NAME, CA.Street_Address, CA.Postal_Code, CA.Country
FROM Customer_Addresses AS CA
LEFT JOIN Customers AS C ON CA.Customer_Id = C.ID;


