import CustomerAddress from "./CustomerAddress";

export default class Customer {
    private Name: string;
    private Address: Array<CustomerAddress>;

    constructor(name: string, address: Array<CustomerAddress>){
        this.Name = name;
        this.Address = address;
    }
}