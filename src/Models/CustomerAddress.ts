export default class CustomerAddress {
    private Country: string;
    private PostalCode: number;
    private StreetAddress: string;

    constructor(
        country: string,
        postalCode: number,
        streetAddress: string
    ){
        this.Country = country;
        this.PostalCode = postalCode;
        this.StreetAddress = streetAddress;
    }
}