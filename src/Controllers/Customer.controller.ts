import { Request, Response } from 'express';
import { inject } from 'inversify';
import { 
    interfaces, 
    controller, 
    httpGet,
    requestParam,
    response
} from "inversify-express-utils";

import { ICustomerService }  from '../Services/Interfaces/ICustomerService';
import { TYPES } from '../types';

@controller("/customer")
export class CustomerController implements interfaces.Controller{

    private readonly _customerService: ICustomerService;
    private readonly _errorMessage: string = "There was an error with your request.";

    public constructor(@inject(TYPES.ICustomerService) customerService: ICustomerService){
        this._customerService = customerService;
    }

    @httpGet("/")
    public async getAllCustomers(req: Request, @response() res: Response): Promise<void> {
        try{
            res.status(200).json({ 
                customers: await this._customerService.getAllCustomers()
            });
        }
        catch (ex){
            this.errorHandler(res);
        }
    }

    @httpGet("/find/:id")
    public async getCustomerById(@requestParam("id") id: string, @response() res: Response): Promise<void> {
        try{
            res.status(200).json({ 
                customer: await this._customerService.getCustomerById(id)
            });
        }
        catch (ex){
            this.errorHandler(res, "Customer not found.");
        }
    }

    @httpGet("/withaddresses")
    public async getCustomersWithAddresses(req: Request, @response() res: Response): Promise<void> {
        try{
            res.status(200).json({ 
                customers: await this._customerService.getCustomersWithAddresses()
            });
        }
        catch (ex){
            this.errorHandler(res);
        }
    }

    private errorHandler(res: Response, msg?: string){
        res.status(404).json({ error: msg || this._errorMessage });
    }
}