import { Container } from 'inversify';
import { TYPES } from './types';
import { ICustomerService } from './Services/Interfaces/ICustomerService';
import { CustomerService } from './Services/Customer.service';
import DbContext from './Services/DbContext';
import { IDbContext } from './Services/Interfaces/IDbContext';

class IoC {
    public container: Container;

    constructor(){
        this.container = new Container();
        this.register();
    }
    
    register(){
        this.container.bind<IDbContext>(TYPES.IDbContext).to(DbContext);
        this.container.bind<ICustomerService>(TYPES.ICustomerService).to(CustomerService);
    }
}

export const container = new IoC().container;