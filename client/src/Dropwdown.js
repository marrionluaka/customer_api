import React, { Component } from 'react';

export default class Dropdown extends Component {
    state = {
        value: "All"
    };

    change = e => {
        this.props.onChange(e.target.value);
        this.setState({value: e.target.value});
    }

    render(){
        return(
            <select onChange={this.change} value={this.state.value}>
                <option value="all">All</option>
                <option value="wa">With Address</option>
            </select>
        )
    }
}