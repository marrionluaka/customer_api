import React, { Component } from 'react';
import './App.css';
import axios from 'axios';

import Dropdown from './Dropwdown';

class App extends Component {
  state = {
    customers: []
  };

  componentDidMount() {
    axios.get('http://localhost:4000/customer')
      .then(res => {
        this.setState({ customers: res.data.customers });
      });
  }

  onChange = value => {
    var url = value === "all" ? 
      'http://localhost:4000/customer' : 'http://localhost:4000/customer/withaddresses';

    axios.get(url)
        .then(res => {
            this.setState({ customers: res.data.customers });
        });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Dropdown onChange={this.onChange}/>
          <table style={{width: "100%" }}>
            <thead>
              <tr>
                <th>Name</th>
                <th>StreetAddress</th>
                <th>PostalCode</th>
                <th>Country</th>
              </tr>
            </thead>
            <tbody>
              {
                this.state.customers.map((c, idx) => {
                  let isAddressAvailable = !!c.Address;
                  return (
                    <tr key={idx}>
                      <td>{c.Name}</td>
                      <td>{isAddressAvailable ? c.Address[0].StreetAddress : "N/A"}</td>
                      <td>{isAddressAvailable ? c.Address[0].PostalCode : "N/A"}</td>
                      <td>{isAddressAvailable ? c.Address[0].Country : "N/A"}</td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
        </header>
      </div>
    );
  }
}

export default App;
